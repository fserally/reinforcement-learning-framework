# Reinforcement Learning Framework

This project contains a set of experimentations for the turtlebot3 to carry out simple reinforcement learning tasks using Qlearning from OpenAI Ros, deep reinforcement learning tasks using PPO and A2C from stable baselines. Possible to carry out incremental learning. The experiments were carried out on 2 gazebo worlds and on a real turtlebot3.

## Installation

Dependencies: requires ROS Noetic, Turtlebot3 packages for Noetic, Turtlebot3 Autorace packages

## Installation of ROS Noetic: 

Setup your computer to accept software from packages.ros.org. 

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' 

Setup your keys 

sudo apt install curl  

curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add - 

Make sure Debian package index is up to date: 

sudo apt update 

Install ROS Noetic (http://wiki.ros.org/noetic/Installation/Ubuntu) 

Desktop-Full Install: (Recommended) : Everything in Desktop plus 2D/3D simulators and 2D/3D perception packages 

sudo apt install ros-noetic-desktop-full 

Automatically source this script every time a new shell is launched: 

echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc 

source ~/.bashrc 

Install dependencies: 

sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential 

Initialize rosdep: 

sudo apt install python3-rosdep  

sudo rosdep init 

rosdep update 

 
Create virtual environment (from Pycharms).  

Note: if you are using my PC, activate the virtual environment already installed with the command source ~/fatimah/venvpy/bin/activate 

Install python 3.7: sudo apt install python3.7 

Install Keras: pip install tensorflow & pip install keras-rl 

## Install turtlebot3 packages for ROS Noetic 

sudo apt-get install ros-noetic-joy ros-noetic-teleop-twist-joy \ 

  ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc \ 

  ros-noetic-rgbd-launch ros-noetic-rosserial-arduino \ 

  ros-noetic-rosserial-python ros-noetic-rosserial-client \ 

  ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server \ 

  ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro \ 

  ros-noetic-compressed-image-transport ros-noetic-rqt* ros-noetic-rviz \ 

  ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers  

sudo apt install ros-noetic-dynamixel-sdk 

sudo apt install ros-noetic-turtlebot3-msgs 

sudo apt install ros-noetic-turtlebot3 

## Install turtlebot3 autorace packages 

Turtlebot3 does not come with a camera, so I have used the autorace model. 

Install the autorace package:  

git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_autorace_2020.git 

cd ~/catkin_ws && catkin_make 

sudo apt install ros-noetic-image-transport ros-noetic-cv-bridge ros-noetic-vision-opencv python3-opencv libopencv-dev ros-noetic-image-proc 

## Configuration of catkin workspace  

Create catkin workspace: 

mkdir -p ~/catkin_ws/src 

cd ~/catkin_ws/ 

catkin_make 

echo "export TURTLEBOT3_MODEL=burger" >> ~/.bashrc 

source ~/.bashrc 

echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc 

source ~/.bashrc 

## Install Turtlebot3 simulation package 

cd ~/catkin_ws/src/ 

git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git 

cd ~/catkin_ws && catkin_make 

## Installation of OpenAI Gym and OpenAI Ros (in virtual environment) 

pip install gym 

cd ~/catkin_ws/src

git clone --branch version2 https://bitbucket.org/theconstructcore/openai_ros.git 
```
rosdep install openai_ros 
```
If below error occurs: 

error openai_ros: No definition of [python-catkin-pkg] for OS version [focal] 

change the line 14 in package.xml in package openai_ros from

<build_depend>python-catkin-pkg</build_depend> to <build_depend>python3-catkin-pkg</build_depend> 

```
catkin_make 
source devel/setup.bash 
```

In the virtual environment, install the following dependencies 

```
pip install defusedxml 

pip install pyaml rospkg  

pip install torch torchvision  

pip install --upgrade cython  

pip install tqdm cython pycocotools  

pip install matplotlib  

pip install opencv-python  

pip uninstall em  

pip install empy 

pip install gitpython 

pip install visdom 

pip install pyaml rospkg 

sudo apt-get install ros-noetic-cv-camera 

sudo apt install ros-noetic-image-geometry 

pip install opencv-python 

Pip install cv_bridge
```

## Installation of stable baselines 

```
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev 

pip install tensorflow==1.14.0  

pip install stable-baselines[mpi]==2.10.0 
```


## Add your files

```
Cd in src of catkin_ws workspace 
git remote add origin https://gitlab.com/fserally/reinforcement-learning-framework.git
git clone https://gitlab.com/fserally/reinforcement-learning-framework.git . 
git branch -M main
git push -uf origin main
catkin_make 
source devel/setup.bash 
```


## Usage


## Experiment 1 - Turtlebot in a maze

## Training with Qlearning

In a new terminal, launch roscore 

In a new terminal, in the virtual environment, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment, launch the training script with 

roslaunch epan_rl_framework  start_training_v2.launch 

## Evaluation with Qlearning

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_simulation_v2.launch 

## Evaluation with Qlearning with real turtlebot 

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, ssh on turtlebot and launch roslaunch turtlebot3_bringup turtlebot3_robot.launch 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_real_v2.launch 

## Training with PPO

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the training script with 

roslaunch epan_rl_framework  start_training_dqn_v2.launch 

## Evaluation with PPO in simulation

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_dqn_simulation_v2.launch 

## Evaluation with PPO with real turtlebot

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_dqn_real_v2.launch 

## Experiment 2 - Turtlebot vision based navigation

## Training with A2C

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the training script with 

roslaunch epan_rl_framework  start_training_dqn_images_v2.launch

## Evaluation with A2C in simulation

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_dqn_images_simulation_v2.launch 

## Evaluation with A2C with real turtlebot

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_dqn_images_real_v2.launch 

## Experiment 3 - Visual Servoing

## Training with A2C

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the training script with 

roslaunch epan_rl_framework  start_training_dqn_poster_v2.launch 

## Evaluation with A2C

In a new terminal, launch roscore 

In a new terminal, in the virtual environment created above, launch visdom server with 

python -m visdom.server 

In a new terminal, in the virtual environment created above, launch the evaluation script with 

roslaunch epan_rl_framework  start_evaluation_dqn_poster_simulation_v2.launch 



