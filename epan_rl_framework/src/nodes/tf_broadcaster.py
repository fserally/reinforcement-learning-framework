#!/usr/bin/env python
import roslib
import rospy
import tf
import math
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState, GetModelState

if __name__ == '__main__':
    rospy.init_node('my_tf_broadcaster')
    br = tf.TransformBroadcaster()
    listener = tf.TransformListener()
    rate = rospy.Rate(30.0)
    while not rospy.is_shutdown():
        try:
            br.sendTransform((0, 0, 0),
                            tf.transformations.quaternion_from_euler(0, 0, 0),
                            rospy.Time.now(),
                            "world",
                            "odom")
            '''br.sendTransform((0.040, -0.011, 0.130),
                            tf.transformations.quaternion_from_euler(0, 0.174, 0),
                            rospy.Time.now(),
                            "camera_link",
                            "base_link")
            br.sendTransform((0.003, 0.011, 0.009),
                            tf.transformations.quaternion_from_euler(0, 0, 0),
                            rospy.Time.now(),
                            "camera_rgb_frame",
                            "camera_link")

            br.sendTransform((0.0, 0.0, 0.0),
                            tf.transformations.quaternion_from_euler(-1.57, 0, -1.57),
                            rospy.Time.now(),
                            "camera_rgb_optical_frame",
                            "camera_rgb_frame")'''
            

            
            rospy.wait_for_service('/gazebo/get_model_state')
            get_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            resp_poster = get_state('poster','')
            poster_pose = resp_poster.pose

            br.sendTransform((poster_pose.position.x, poster_pose.position.y, poster_pose.position.z),
                                    (poster_pose.orientation.x, poster_pose.orientation.y, poster_pose.orientation.z, poster_pose.orientation.w),
                                    rospy.Time.now(),
                                    "poster_link",
                                    "world")

            rate.sleep()
        except rospy.ROSInterruptException:
            rospy.logerr("ROS Interrupt Exception! Just ignore the exception!")
        except rospy.ROSTimeMovedBackwardsException:
            rospy.logerr("ROS Time Backwards! Just ignore the exception!")
        #rospy.sleep(0.5)



