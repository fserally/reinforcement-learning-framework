#!/usr/bin/env python
import json
import gym
import numpy
import time
import os

from past.builtins import raw_input

import qlearn
import visdom
from gym import wrappers
# ROS packages required
import rospy
import numpy as np

import pickle
import itertools
import rospkg
import os
import os.path
from os import path
from datetime import datetime
from stable_baselines.common.policies import MlpPolicy
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3 import A2C, SAC, PPO, TD3
from openai_ros.openai_ros_common import StartOpenAI_ROS_Environment
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines.common.callbacks import EvalCallback, StopTrainingOnRewardThreshold
from stable_baselines import DQN
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
import random
import pandas
from glob import glob




#callback fired after each step
class StepCallback(BaseCallback):

    def __init__(self, check_freq, episodes_threshold, log_dir, previous_monitoring_results, verbose=1):
        super(StepCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        self.best_mean_reward = -np.inf
        self.episodes_threshold = episodes_threshold
        self.previous_results = previous_monitoring_results


    def _on_step(self) -> bool:
        
        # Retrieve training reward - retrieve entries from log file
        if run_mode == 'incremental_learning':
            current_data = load_results(self.log_dir)
            data_frame = pandas.concat([self.previous_results, current_data])
        else:
            data_frame = load_results(self.log_dir)

        episodes, rewards = ts2xy(data_frame, 'episodes')


        if len(episodes) > 0:
            current_episode = episodes[-1]
        else:
            current_episode = 0

        if current_episode > self.episodes_threshold:
            return False

        ###### plot graph #####

        labels = []
        for i in range(int(len(rewards))): labels.append(i)
        avg_data = []
        # average
        average = 50
        for i, val in enumerate(rewards):
            if i % average == 0:
                if (i + average) < len(rewards) + average:
                    avg = sum(rewards[i:i + average]) / average
                    avg_data.append(avg)
        average = expand(avg_data, average)
        average = average[:len(rewards)]
        average_labels = []


        for i in range(int(len(average))): average_labels.append(i)

        if len(rewards) > 0:
            vis.line(
                X=np.column_stack((labels, average_labels)),
                Y=np.column_stack((rewards, average)),
                win=win,
                opts=dict(
                    title="Evaluation Results" if run_mode == "evaluation" else "Training Results",
                    legend=["results", "Average"],
                )
            )

        # save model each 100 episodes
        if (current_episode + 1) % 1000 == 0 and current_episode > 0:
            model_name = "turtlebot3_" + str(current_episode + 1)
            model_name_path = os.path.join(outdir_model, model_name)
            self.model.save(model_name_path)
        
        return True

'''In the case of incremental learning, we need to load data from the log files then append to the current log 
to continue to plot the same graph on visdom so that data is not lost'''
def load_monitor_results():
    training_model_path = rospy.get_param("/turtlebot3/training_model_path")
    base_path = training_model_path.split("/")[0]
    path = pkg_path + '/src/models/' + base_path 
    previous_model = training_model_path.split('/turtlebot3_')[1]
    monitor_files = glob(os.path.join(path, "*old_log.csv")) #get the previous logs
    if len(monitor_files) == 0:
        return False
    data_frames, headers = [], []
    for file_name in monitor_files:
        with open(file_name, "rt") as file_handler:
            first_line = file_handler.readline()
            assert first_line[0] == "#"
            header = json.loads(first_line[1:])
            data_frame = pandas.read_csv(file_handler, index_col=None)
            headers.append(header)
            data_frame["t"] += header["t_start"]

        data_frames.append(data_frame)

    data_frame = pandas.concat(data_frames)
    data_frame = data_frame.head(int(previous_model) + 1)

    data_frame.sort_values("t", inplace=True)
    data_frame.reset_index(inplace=True)
    data_frame["t"] -= min(header["t_start"] for header in headers)
    return data_frame

def createModelDirectory():
    pkg_path = rospack.get_path('epan_rl_framework')
    today = datetime.now()
    if today.hour < 12:
        h = "00"
    else:
        h = "12"

    folder_name = 'model_baselines_a2c_poster' + today.strftime('%Y%m%d_%H:%M:%S') + h
    folder_path = pkg_path + '/src/models/' + folder_name
    os.mkdir(folder_path)
    return folder_path


def expand(lst, n):
    lst = [[i] * n for i in lst]
    lst = list(itertools.chain.from_iterable(lst))
    return lst

#This function is related to Optuna. 
def optimize_ppo(trial):
    """ Learning hyperparamters we want to optimise"""
    return {
        'n_epochs': int(trial.suggest_loguniform('n_epochs', 16, 2048)),
        #'batch_size': int(trial.suggest_int('batch_size', 32, 1024)),
        'gamma': trial.suggest_loguniform('gamma', 0.9, 0.9999),
        'learning_rate': trial.suggest_loguniform('learning_rate', 1e-5, 1.),
        'ent_coef': trial.suggest_loguniform('ent_coef', 1e-8, 1e-1),
        'clip_range': trial.suggest_uniform('clip_range', 0.1, 0.4),
        'gae_lambda': trial.suggest_uniform('gae_lambda', 0.8, 1.)
    }

#This function is related to Optuna
def optimize_agent(trial):
    """ Train the model and optimize
        Optuna maximises the negative log likelihood, so we
        need to negate the reward here
    """
    model_params = optimize_ppo(trial)
    model = PPO('MultiInputPolicy', env, verbose=1, batch_size= 64, **model_params)
    model.learn(int(1e10), callback=callback)
    mean_reward, std_reward = evaluate_policy(model, env, n_eval_episodes=10)
    print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")

    return -1 * mean_reward

if __name__ == '__main__':

    run_mode = rospy.get_param("/turtlebot3/run_mode")
    vis = visdom.Visdom()  # for visualization in real time
    win = "win"
    
    if run_mode == "training":
        win = vis.line(
            X=np.column_stack(([0], [0])),
            Y=np.column_stack(([0],
                               [0])),
            win=win,
        )


    rospy.init_node('turtlebot3_dqn_baselines_poster', anonymous=True, log_level=rospy.DEBUG)

    if run_mode == "training" or run_mode == 'incremental_learning':
        episodes_threshold = rospy.get_param("/turtlebot3/episodes_training")
    else:
        episodes_threshold = rospy.get_param("/turtlebot3/episodes_running")

    # Init OpenAI_ROS ENV
    task_and_robot_environment_name = rospy.get_param(
        '/turtlebot3/task_and_robot_environment_name')
    env = StartOpenAI_ROS_Environment(
        task_and_robot_environment_name, 700)

    # Create the Gym environment

    rospy.loginfo("Gym environment done")

    # Set the logging system
    rospack = rospkg.RosPack()
    pkg_path = rospack.get_path('epan_rl_framework')
    outdir = pkg_path + '/src/training_results'
    previous_monitoring_results = None

    if run_mode == "training":
        outdir_model = createModelDirectory()
    elif run_mode == 'incremental_learning':
        training_model_path = rospy.get_param("/turtlebot3/training_model_path")
        base_path = training_model_path.split("/")[0]
        outdir_model = pkg_path + '/src/models/' + base_path
        #rename monitor log to keep previous results
        today = datetime.now()
        if path.exists(pkg_path + '/src/models/' + base_path + '/monitor.csv'):
            os.rename(pkg_path + '/src/models/' + base_path + '/monitor.csv', pkg_path + '/src/models/' + base_path + '/' + today.strftime('%Y%m%d_%H:%M:%S') + '_old_log.csv')

        previous_monitoring_results = load_monitor_results()
    else:
        outdir_model = pkg_path + '/src/models/'

    env = Monitor(env, outdir_model)
    env = DummyVecEnv([lambda: env])

    callback = StepCallback(check_freq=2, episodes_threshold=episodes_threshold, log_dir=outdir_model, previous_monitoring_results = previous_monitoring_results, verbose=1)
    
    start_time = time.time()
    rospy.logwarn("############### Start Training ###############")
    
    if run_mode == 'training':
        rospy.loginfo("Starting Learning")
        model = A2C('MultiInputPolicy', env, verbose=1).learn(int(1e10), callback=callback)
    elif run_mode == 'incremental_learning':
        rospy.loginfo("Starting Incremental Learning")
        model = A2C.load(pkg_path + '/src/models/' + training_model_path)
        model.set_env(env)
        model.learn(int(1e10), callback=callback)
    else:
        # Load the trained agent
        evaluation_model_path = rospy.get_param("/turtlebot3/evaluation_model_path")
        model = A2C.load(pkg_path + '/src/models/' + evaluation_model_path)
        # Evaluate the agent

        mean_reward, std_reward = evaluate_policy(model, env, n_eval_episodes=episodes_threshold)
        print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")

        '''
        Testing with Optuna
        rospy.loginfo("Starting Optuna")
        study = optuna.create_study()
        try:
            study.optimize(optimize_agent, n_trials=3, n_jobs=4)
            optuna.visualization.plot_intermediate_values(study).show(renderer="browser")
            optuna.visualization.plot_optimization_history(study).show(renderer="browser")
            optuna.visualization.plot_param_importances(study).show(renderer="browser")
            optuna.visualization.plot_optimization_history(study).show(renderer="browser")
        except KeyboardInterrupt:
            print('Interrupted by keyboard.')
        '''

