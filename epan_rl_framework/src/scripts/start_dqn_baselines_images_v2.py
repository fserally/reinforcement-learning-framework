#!/usr/bin/env python
import json
import gym
import numpy
import time
import os

from past.builtins import raw_input

import qlearn
import visdom
from gym import wrappers
# ROS packages required
import rospy
import numpy as np

import pickle
import itertools
import rospkg
import os
from datetime import datetime
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines3 import A2C, SAC, PPO, TD3
from openai_ros.openai_ros_common import StartOpenAI_ROS_Environment
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines.common.callbacks import EvalCallback, StopTrainingOnRewardThreshold
from stable_baselines import DQN
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
from stable_baselines3 import A2C
import random

#callback fired after each step
class StepCallback(BaseCallback):

    def __init__(self, check_freq, episodes_threshold, log_dir, verbose=1):
        super(StepCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        self.best_mean_reward = -np.inf
        self.episodes_threshold = episodes_threshold

    def _on_step(self) -> bool:
        # Retrieve training reward - retrieve entries from log file
        episodes, rewards = ts2xy(load_results(self.log_dir), 'episodes')
        if len(episodes) > 0:
            current_episode = episodes[-1]
        else:
            current_episode = 0

        #stop the training if we reached the episode threshold
        if current_episode > self.episodes_threshold:
            return False

        ###### plot graph #####

        labels = []
        for i in range(int(len(rewards))): labels.append(i)
        avg_data = []
        # average line data
        average = 50
        for i, val in enumerate(rewards):
            if i % average == 0:
                if (i + average) < len(rewards) + average:
                    avg = sum(rewards[i:i + average]) / average
                    avg_data.append(avg)
        average = expand(avg_data, average)
        average = average[:len(rewards)]
        average_labels = []
        for i in range(int(len(average))): average_labels.append(i)

        if len(rewards) > 0:
            vis.line(
                X=np.column_stack((labels, average_labels)),
                Y=np.column_stack((rewards, average)),
                win=win,
                opts=dict(
                    title="Training Results" if run_mode == "training" else "Evaluation Results",
                    legend=["results", "Average"],
                )
            )

        # save model each 100 episodes
        if run_mode == "training":
            if (current_episode + 1) % 100 == 0 and current_episode > 0:
                model_name = "turtlebot3_" + str(current_episode + 1)
                model_name_path = os.path.join(outdir_model, model_name)
                self.model.save(model_name_path)

        return True


#create model directory to save weights
def createModelDirectory():
    pkg_path = rospack.get_path('epan_rl_framework')
    today = datetime.now()
    if today.hour < 12:
        h = "00"
    else:
        h = "12"

    folder_name = 'model_baselines_a2c' + today.strftime('%Y%m%d_%H:%M:%S') + h
    folder_path = pkg_path + '/src/models/' + folder_name
    os.mkdir(folder_path)
    return folder_path


def expand(lst, n):
    lst = [[i] * n for i in lst]
    lst = list(itertools.chain.from_iterable(lst))
    return lst




if __name__ == '__main__':

    vis = visdom.Visdom()  # for visualization in real time
    win = "win"
    win = vis.line(
        X=np.column_stack(([0], [0])),
        Y=np.column_stack(([0],
                           [0])),
        win=win,
    )

    # initialize layout of line graph

    trace_loss = dict(x=[], y=[], mode="markers+lines", type='custom',
                      marker={'color': 'red', 'symbol': 104, 'size': "10"},
                      text=["one", "two", "three"], name='2nd Trace')
    layout = dict(title="Training Results", xaxis={'title': 'Episodes'}, yaxis={'title': 'Total Rewards'})

    layout_loss = dict(title="Decreasing probability of choosing a random action", xaxis={'title': 'Episodes'},
                       yaxis={'title': 'Epsilon'})
    

    rospy.init_node('turtlebot3_dqn_baselines_images', anonymous=True, log_level=rospy.DEBUG)
    nsteps = rospy.get_param("/turtlebot3/nsteps")
    run_mode = rospy.get_param("/turtlebot3/run_mode")
    evaluation_model_path = rospy.get_param("/turtlebot3/evaluation_model_path")


    if run_mode == "training":
        episodes_threshold = rospy.get_param("/turtlebot3/episodes_training")
    else:
        episodes_threshold = rospy.get_param("/turtlebot3/episodes_running")

    # Init OpenAI_ROS ENV
    task_and_robot_environment_name = rospy.get_param(
        '/turtlebot3/task_and_robot_environment_name')
    env = StartOpenAI_ROS_Environment(
        task_and_robot_environment_name, 700)

    # Create the Gym environment

    rospy.loginfo("Gym environment done")

    # set random position

    rospy.loginfo("Starting Learning")

    # Set the logging system
    rospack = rospkg.RosPack()
    pkg_path = rospack.get_path('epan_rl_framework')
    outdir = pkg_path + '/src/training_results'

    if run_mode == "training":
        outdir_model = createModelDirectory()
    else:
        outdir_model = pkg_path + '/src/models/'

    env = Monitor(env, outdir_model)
    env = DummyVecEnv([lambda: env])

    callback = StepCallback(check_freq=2, episodes_threshold=episodes_threshold, log_dir=outdir_model, verbose=1)
   

    start_time = time.time()
    rospy.logwarn("############### Start Training ###############")
    
    if run_mode == 'training':
        model = A2C('CnnPolicy', env, verbose=1).learn(int(1e10), callback=callback)
    else:
        # Load the trained agent
        model = A2C.load(pkg_path + '/src/models/' + evaluation_model_path)
        # Evaluate the agent
        mean_reward, std_reward = evaluate_policy(model, env, n_eval_episodes=episodes_threshold)
        print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")
