import rospy
import numpy
from gym import spaces
from gym.envs.registration import register
from geometry_msgs.msg import Vector3
from openai_ros.task_envs.task_commons import LoadYamlFileParamsTest
from openai_ros.openai_ros_common import ROSLauncher
import os
import numpy as np
from openai_ros.robot_envs.turtlebot3_robotic_lab_real_env import TurtleBot3RoboticLabRealEnv
import cv2

class TurtleBot3RoboticLabRealWorldEnv(TurtleBot3RoboticLabRealEnv):
    def __init__(self):
        """
       This Task Env is designed for having the TurtleBot3 in the robotic lab real world
        It will learn how to move in the direction of red pixels
        """
        # This is the path where the simulation files, the Task and the Robot gits will be downloaded if not there
        ros_ws_abspath = rospy.get_param("/turtlebot3/ros_ws_abspath", None)
        assert ros_ws_abspath is not None, "You forgot to set ros_ws_abspath in your yaml file of your main RL script. Set ros_ws_abspath: \'YOUR/SIM_WS/PATH\'"
        assert os.path.exists(ros_ws_abspath), "The Simulation ROS Workspace path " + ros_ws_abspath + \
                                               " DOESNT exist, execute: mkdir -p " + ros_ws_abspath + \
                                               "/src;cd " + ros_ws_abspath + ";catkin_make"

        '''ROSLauncher(rospackage_name="turtlebot3_gazebo",
                    launch_file_name="turtlebot3_robotic_lab.launch",
                    ros_ws_abspath=ros_ws_abspath)
        '''
        # Load Params from the desired Yaml file
        LoadYamlFileParamsTest(rospackage_name="openai_ros",
                               rel_path_from_package_to_file="src/openai_ros/task_envs/turtlebot3/config",
                               yaml_file_name="turtlebot3_robotic_lab_real_world.yaml")


        # Here we will add any init functions prior to starting the MyRobotEnv
        super(TurtleBot3RoboticLabRealWorldEnv, self).__init__(ros_ws_abspath)

        # Only variable needed to be set here
        number_actions = rospy.get_param('/turtlebot3/n_actions')
        self.action_space = spaces.Discrete(number_actions)
        self.reward_range = (-numpy.inf, numpy.inf)
        # initialize variables for number of pixels detection
        self.previous_num_pixels = 0
        self.current_num_pixels = 0


        #number_observations = rospy.get_param('/turtlebot3/n_observations')
        """
        We set the Observation space for the 6 observations
        cube_observations = [
            round(current_disk_roll_vel, 0),
            round(y_distance, 1),
            round(roll, 1),
            round(pitch, 1),
            round(y_linear_speed,1),
            round(yaw, 1),
        ]
        """

        # Actions and Observations
        self.linear_forward_speed = rospy.get_param('/turtlebot3/linear_forward_speed')
        self.linear_turn_speed = rospy.get_param('/turtlebot3/linear_turn_speed')
        self.angular_speed = rospy.get_param('/turtlebot3/angular_speed')
        self.init_linear_forward_speed = rospy.get_param('/turtlebot3/init_linear_forward_speed')
        self.init_linear_turn_speed = rospy.get_param('/turtlebot3/init_linear_turn_speed')

        self.new_ranges = rospy.get_param('/turtlebot3/new_ranges')
        self.min_range = rospy.get_param('/turtlebot3/min_range')
        self.max_laser_value = rospy.get_param('/turtlebot3/max_laser_value')
        self.min_laser_value = rospy.get_param('/turtlebot3/min_laser_value')
        self.max_linear_aceleration = rospy.get_param('/turtlebot3/max_linear_aceleration')
        self.learning_algorithm = rospy.get_param("/turtlebot3/learning_algorithm", None)
        self.threshold_goal_pixel = rospy.get_param('/turtlebot3/threshold_goal_pixel')
        rospy.logdebug("get camera image")
        cv_image = self.get_camera_image()
        height, width, channels = cv_image.shape

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.
        laser_scan = self.get_laser_scan()
        if self.learning_algorithm == 'deepRL':
            num_laser_readings = 5
        else:
            num_laser_readings = int(len(laser_scan.ranges)/self.new_ranges)

        high = numpy.full((num_laser_readings), self.max_laser_value)
        low = numpy.full((num_laser_readings), self.min_laser_value)

        # We only use two integers
        self.observation_space = spaces.Box(low=0, high=255, shape=(height, width, channels), dtype=np.uint8)

        rospy.logdebug("ACTION SPACES TYPE===>"+str(self.action_space))
        rospy.logdebug("OBSERVATION SPACES TYPE===>"+str(self.observation_space))

        # Rewards
        self.forwards_reward = rospy.get_param("/turtlebot3/forwards_reward")
        self.turn_reward = rospy.get_param("/turtlebot3/turn_reward")
        self.increased_pixel_count_reward = rospy.get_param("/turtlebot3/increased_pixel_count_reward")
        self.decreased_pixel_count_reward = rospy.get_param("/turtlebot3/decreased_pixel_count_reward")
        self.end_episode_points = rospy.get_param("/turtlebot3/end_episode_points")
        self.end_episode_points = rospy.get_param("/turtlebot3/end_episode_points")
        self.goal_reached_points = rospy.get_param("/turtlebot3/goal_reached")
        self.cumulated_steps = 0.0
        self.cumulated_steps = 0.0
        


    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        rospy.logerr("Init pose")
        self.move_base( self.init_linear_forward_speed,
                        self.init_linear_turn_speed,
                        epsilon=0.05,
                        update_rate=10)

        return True


    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        # For Info Purposes
        self.cumulated_reward = 0.0
        # Set to false Done, because its calculated asyncronously
        self._episode_done = False
        # Set current red pixels to 0
        self.current_num_pixels = 0


    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the turtlebot2
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """

        rospy.logdebug("Start Set Action ==>"+str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv
        if action == 0: #FORWARD
            linear_speed = self.linear_forward_speed
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 1: #LEFT
            linear_speed = self.linear_turn_speed
            angular_speed = self.angular_speed
            self.last_action = "TURN_LEFT"
        elif action == 2: #RIGHT
            linear_speed = self.linear_turn_speed
            angular_speed = -1*self.angular_speed
            self.last_action = "TURN_RIGHT"
        '''elif action == 3:  # BACKWARDS
            linear_speed = -1*self.linear_forward_speed
            angular_speed = 0.0
            self.last_action = "BACKWARDS"
        '''

        # We tell TurtleBot2 the linear and angular speed to set to execute
        self.move_base(linear_speed, angular_speed, epsilon=0.05, update_rate=10)

        rospy.logdebug("END Set Action ==>"+str(action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        :return:
        """
        rospy.logdebug("Start Get Observation ==>")
        # We get the laser scan data
        laser_scan = self.get_laser_scan()
        # check if robot has crashed
        discretized_observations = self.discretize_scan_observation(    laser_scan,
                                                                        self.new_ranges
                                                                        )
        cv_image = self.get_camera_image()
        if self._episode_done:
            return cv_image
        else:
            self.extract_red_pixel(cv_image)

        rospy.logdebug("Observations==>"+str(discretized_observations))
        rospy.logdebug("END Get Observation ==>")
        return cv_image


    def _is_done(self, observations):

        if self._episode_done:
            rospy.logerr("TurtleBot2 is Too Close to wall==>")
            self._episode_done = True
            self.previous_num_pixels = self.current_num_pixels
            # Now we check if it has crashed based on the imu
            imu_data = self.get_imu()
            linear_acceleration_magnitude = self.get_vector_magnitude(imu_data.linear_acceleration)
            if linear_acceleration_magnitude < self.max_linear_aceleration:
                rospy.logerr("TurtleBot2 Crashed==>"+str(linear_acceleration_magnitude)+">"+str(self.max_linear_aceleration))
                self._episode_done = True
            else:
                rospy.logerr("DIDNT crash TurtleBot2 ==>"+str(linear_acceleration_magnitude)+"<"+str(self.max_linear_aceleration))
                #self._episode_done = False

        else:
            self._episode_done = False
            rospy.logwarn("TurtleBot2 is NOT close to a wall ==>")



        return self._episode_done

    def _compute_reward(self, observations, done):

        if not done:
            if self.previous_num_pixels < self.current_num_pixels:
                reward = self.increased_pixel_count_reward
            else:
                reward = self.decreased_pixel_count_reward
        else:
            if self.current_num_pixels < self.threshold_goal_pixel:
                reward = -1 * self.end_episode_points
            else:
                reward = self.goal_reached_points

        rospy.logdebug("reward=" + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward=" + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps=" + str(self.cumulated_steps))
        # assign previous number of pixels to current for next step
        self.previous_num_pixels = self.current_num_pixels

        return reward


    # Internal TaskEnv Methods

    def extract_red_pixel(self, cv_image):
        """
        Extract and count the number of red pixels
        """
        try:
            maskRed = cv2.inRange(cv_image, (0,0,112), (92,92,160))
            maskPink = cv2.inRange(cv_image, (24,0,150), (89,63,227))
            mask = cv2.bitwise_or(maskRed, maskPink )
    
            output = cv2.bitwise_and(cv_image, cv_image, mask=mask)
            cv2.imshow("Image cv_image", cv_image)
            cv2.imshow("Image window", output)
            cv2.waitKey(1)
            num_red_pixel = np.count_nonzero(output)
            self.current_num_pixels = num_red_pixel
            if num_red_pixel == 0 or num_red_pixel >= 80000:
                self._episode_done = True
            rospy.logdebug("Current number of red pixels=" + str(self.current_num_pixels))
            rospy.logdebug("Previous number of red pixels=" + str(self.previous_num_pixels))
            print("number of red pixel" + str(num_red_pixel))
        except:
            rospy.logerr("Problem extracting red pixels from image")

    def discretize_scan_observation(self,data,new_ranges):
        """
        Discards all the laser readings that are not multiple in index of new_ranges
        value.
        """
        self._episode_done = False

        discretized_ranges = []
        mod = len(data.ranges)/new_ranges

        rospy.logdebug("new_ranges=" + str(new_ranges))
        rospy.logdebug("mod=" + str(mod))

        for i, item in enumerate(data.ranges):
            if (i%mod==0):
                if item == float ('Inf') or numpy.isinf(item):
                    discretized_ranges.append(self.max_laser_value)
                elif numpy.isnan(item):
                    discretized_ranges.append(self.min_laser_value)
                else:
                    if self.learning_algorithm == 'deepRL':
                        discretized_ranges.append(item)
                    else:
                        discretized_ranges.append(int(item))

            if (self.min_range > item > 0):
                rospy.logerr("done Validation >>> item=" + str(item)+"< "+str(self.min_range))
                self._episode_done = True
            #else:
                #rospy.logdebug("NOT done Validation >>> item=" + str(item)+"< "+str(self.min_range))

        if self.learning_algorithm == 'deepRL':
            return np.array(discretized_ranges).astype(np.float32)
        else:
            return discretized_ranges


    def get_vector_magnitude(self, vector):
        """
        It calculated the magnitude of the Vector3 given.
        This is usefull for reading imu accelerations and knowing if there has been
        a crash
        :return:
        """
        contact_force_np = numpy.array((vector.x, vector.y, vector.z))
        force_magnitude = numpy.linalg.norm(contact_force_np)

        return force_magnitude

