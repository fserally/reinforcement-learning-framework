import rospy
import numpy
from gym import spaces
from openai_ros.robot_envs import turtlebot3_robotic_lab_env
from gym.envs.registration import register
from geometry_msgs.msg import Vector3
from openai_ros.task_envs.task_commons import LoadYamlFileParamsTest
from sensor_msgs.msg import Image
from openai_ros.openai_ros_common import ROSLauncher
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import Pose
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState, GetModelState
import os
import numpy as np
import math
import cv2
import tf
import time
import rospkg

from datetime import datetime

class TurtleBot3VisualServoingRoboticLabWorldEnv(turtlebot3_robotic_lab_env.TurtleBot3RoboticLabEnv):
    def __init__(self):
        """
        This Task Env is designed for having the TurtleBot3 in the robotic lab world
        It will learn how to move to the desired image pose starting from its current pose
        """
        # This is the path where the simulation files, the Task and the Robot gits will be downloaded if not there

        ros_ws_abspath = rospy.get_param("/turtlebot3/ros_ws_abspath", None)
        target_object = rospy.get_param("/target_object", None)
        assert ros_ws_abspath is not None, "You forgot to set ros_ws_abspath in your yaml file of your main RL script. Set ros_ws_abspath: \'YOUR/SIM_WS/PATH\'"
        assert os.path.exists(ros_ws_abspath), "The Simulation ROS Workspace path " + ros_ws_abspath + \
                                               " DOESNT exist, execute: mkdir -p " + ros_ws_abspath + \
                                               "/src;cd " + ros_ws_abspath + ";catkin_make"

        ROSLauncher(rospackage_name="epan_rl_framework",
                    launch_file_name="turtlebot3_visual_servoing_robotic_lab.launch",
                    ros_ws_abspath=ros_ws_abspath
                    )

        time.sleep(2)

        # Load Params from the desired Yaml file
        LoadYamlFileParamsTest(rospackage_name="openai_ros",
                               rel_path_from_package_to_file="src/openai_ros/task_envs/turtlebot3/config",
                               yaml_file_name="turtlebot3_robotic_lab_world.yaml")

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(TurtleBot3VisualServoingRoboticLabWorldEnv, self).__init__(ros_ws_abspath, target_object)

        # Only variable needed to be set here
        number_actions = rospy.get_param('/turtlebot3/n_actions')
        self.action_space = spaces.Box(low=np.array([-0.15, -0.08]), high=np.array([0.15, 0.08]), dtype=np.float32)

        # We set the reward range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)


        # number_observations = rospy.get_param('/turtlebot3/n_observations')

        # Actions and Observations
        self.linear_forward_speed = rospy.get_param('/turtlebot3/linear_forward_speed')
        self.linear_turn_speed = rospy.get_param('/turtlebot3/linear_turn_speed')
        self.angular_speed = rospy.get_param('/turtlebot3/angular_speed')
        self.init_linear_forward_speed = rospy.get_param('/turtlebot3/init_linear_forward_speed')
        self.init_linear_turn_speed = rospy.get_param('/turtlebot3/init_linear_turn_speed')

        self.new_ranges = rospy.get_param('/turtlebot3/new_ranges')
        self.min_range = rospy.get_param('/turtlebot3/min_range')
        self.max_laser_value = rospy.get_param('/turtlebot3/max_laser_value')
        self.min_laser_value = rospy.get_param('/turtlebot3/min_laser_value')
        self.max_linear_aceleration = rospy.get_param('/turtlebot3/max_linear_aceleration')
        self.learning_algorithm = rospy.get_param("/turtlebot3/learning_algorithm", None)
        self.threshold_goal_distance = rospy.get_param('/turtlebot3/threshold_goal_distance')
        self.target_object = target_object

        self.previous_mean_distance = 0
        self.current_mean_distance = 0


        rospy.logdebug("get camera image")
        cv_image = self.get_camera_image()


        height, width, channels = cv_image.shape

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.
        laser_scan = self.get_laser_scan()

        if self.learning_algorithm == 'deepRL':
            num_laser_readings = 5
        else:
            num_laser_readings = int(len(laser_scan.ranges) / self.new_ranges)

        high = numpy.full((num_laser_readings), self.max_laser_value)
        low = numpy.full((num_laser_readings), self.min_laser_value)

        obs_spaces = {}
        obs_spaces['goal_image'] = spaces.Box(low=0, high=255, shape=(height, width, channels), dtype=np.uint8)
        obs_spaces['current_image'] = spaces.Box(low=0, high=255, shape=(height, width, channels), dtype=np.uint8)

        self.observation_space = spaces.Dict(obs_spaces)

        rospy.logdebug("ACTION SPACES TYPE===>" + str(self.action_space))
        rospy.logdebug("OBSERVATION SPACES TYPE===>" + str(self.observation_space))

        # Rewards
        self.forwards_reward = rospy.get_param("/turtlebot3/forwards_reward")
        self.turn_reward = rospy.get_param("/turtlebot3/turn_reward")
        self.increased_distance_points_reward = rospy.get_param("/turtlebot3/increased_distance_points_reward")
        self.decreased_distance_points_reward = rospy.get_param("/turtlebot3/decreased_distance_points_reward")
        self.end_episode_points = rospy.get_param("/turtlebot3/end_episode_points")
        self.max_episode_steps_points = rospy.get_param("/turtlebot3/max_episode_steps_points")
        self.goal_reached_points = rospy.get_param("/turtlebot3/goal_reached")
        self.max_episode_steps = rospy.get_param("/turtlebot3/nsteps")
        self.cumulated_steps = 0.0
        self.height_poster = rospy.get_param("/turtlebot3/height_poster") #in metres
        self.width_poster = rospy.get_param("/turtlebot3/width_poster") #in metres
        self.current_image = None

        
        self.mean_distance = []


    """
        Gets the image and its corner points
    """
    def get_image_coordinate_points(self):
        #Get the desired goal image first: I*
        camera_info = self.get_camera_info()
        cv_image, cMo = self.getPosterMatrix()
        vector_top_left_corner = [-self.width_poster/2, 0,  self.height_poster/2,  1]
        vector_top_right_corner = [self.width_poster/2, 0, self.height_poster/2, 1]
        vector_bottom_left_corner = [-self.width_poster/2, 0, -self.height_poster/2,  1]
        vector_bottom_right_corner = [self.width_poster/2, 0, -self.height_poster/2,  1]
        pixel_coords_top_left = self.convertToPixelCoordinates(cMo, vector_top_left_corner, camera_info.K)
        pixel_coords_top_right = self.convertToPixelCoordinates(cMo, vector_top_right_corner, camera_info.K)
        pixel_coords_bottom_left = self.convertToPixelCoordinates(cMo, vector_bottom_left_corner, camera_info.K)
        pixel_coords_bottom_right = self.convertToPixelCoordinates(cMo, vector_bottom_right_corner, camera_info.K)
        print("top left" + str(pixel_coords_top_left))
        print("top right" + str(pixel_coords_top_right))
        print("bottom left" + str(pixel_coords_bottom_left))
        print("bottom right" + str(pixel_coords_bottom_right))
        #if both top left and top right points have out of bounds coordinates (must be within 240x320 pixels) then it means that the poster is not in the image plane
        if all(coords_x < 0 or coords_x > 320 for coords_x in [pixel_coords_top_left[0], pixel_coords_top_right[0]]) or all(coords_y < 0 or coords_y > 240 for coords_y in [pixel_coords_top_left[1], pixel_coords_top_right[1]]):
            self._episode_done = True
            rospy.logerr("Poster image lost, episode reset")
            return cv_image, [pixel_coords_top_left,pixel_coords_top_right,pixel_coords_bottom_left,pixel_coords_bottom_right]
        cv2.line(cv_image, (int(pixel_coords_top_left[0]),int(pixel_coords_top_left[1])), (int(pixel_coords_top_right[0]),int(pixel_coords_top_right[1])), (255,127,0), 2)
        cv2.line(cv_image, (int(pixel_coords_bottom_left[0]),int(pixel_coords_bottom_left[1])), (int(pixel_coords_bottom_right[0]),int(pixel_coords_bottom_right[1])), (255,127,0), 2)
        cv2.line(cv_image, (int(pixel_coords_bottom_left[0]),int(pixel_coords_bottom_left[1])), (int(pixel_coords_bottom_right[0]),int(pixel_coords_bottom_right[1])), (255,127,0), 2)
        cv2.line(cv_image, (int(pixel_coords_top_right[0]),int(pixel_coords_top_right[1])), (int(pixel_coords_bottom_right[0]),int(pixel_coords_bottom_right[1])), (255,127,0), 2)
        cv2.line(cv_image, (int(pixel_coords_top_left[0]),int(pixel_coords_top_left[1])), (int(pixel_coords_bottom_left[0]),int(pixel_coords_bottom_left[1])), (255,127,0), 2)

        return cv_image, [pixel_coords_top_left,pixel_coords_top_right,pixel_coords_bottom_left,pixel_coords_bottom_right]


    """
        Gets the pose of the poster in the camera coordinate frame
    """
    def getPosterMatrix(self):

        trans, rot = None, None
        while (trans is None or rot is None) and not rospy.is_shutdown():
            try:
                self.tf_listener.waitForTransform("camera_rgb_optical_frame", "poster_link", rospy.Time(), rospy.Duration(4.0))
                cv_image = self.get_camera_image()
                camera_timestamp = self.get_camera_image_timestamp()
                self.tf_listener.waitForTransform("camera_rgb_optical_frame", "poster_link", camera_timestamp, rospy.Duration(4.0))
                trans, rot = self.tf_listener.lookupTransform("camera_rgb_optical_frame", "poster_link", camera_timestamp)
                poster_matrix = self.tf_listener.fromTranslationRotation(trans, rot)

                return cv_image, poster_matrix
            except Exception as e:
                rospy.logerr("TF is not ready YET...")
                duration_obj = rospy.Duration.from_sec(1.0)
                rospy.sleep(duration_obj)

        return cv_image, poster_matrix

    """
        Converts 3D points to pixel coordinates through perspective projection
    """
    def convertToPixelCoordinates(self, cMo, corner_vector, K):
        cP_corner_point = np.matmul(cMo, corner_vector) #3D point w.r.t camera frame
        cam_K = np.array(K).reshape((3, 3))
        x = cP_corner_point[0] / cP_corner_point[2] # coordinates in image plane
        y = cP_corner_point[1] / cP_corner_point[2]
        image_plan_coordinates = [x, y,1]
        pixel_coordinates = np.matmul(cam_K, image_plan_coordinates)
        return pixel_coordinates

    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        rospy.logdebug("Init pose")

        self.move_base(self.init_linear_forward_speed,
                       self.init_linear_turn_speed,
                       epsilon=0.05,
                       update_rate=10)


        return True

    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        # For Info Purposes
        self.cumulated_reward = 0.0
        # Set to false Done, because its calculated asyncronously
        self._episode_done = False
        self.current_image = None
        self.episode_step = 0
        self.mean_distance = []

    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the turtlebot2
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """

        rospy.logdebug("Start Set Action ==>" + str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv

        linear_speed = action[0]
        angular_speed = action[1]

        if (math.isnan(linear_speed) or math.isinf(linear_speed)):
            linear_speed = 0.22
        if (math.isnan(angular_speed) or math.isinf(angular_speed)):
            angular_speed = 0


        # We tell TurtleBot2 the linear and angular speed to set to execute
        self.move_base(linear_speed, angular_speed, epsilon=0.05, update_rate=10)

        rospy.logdebug("END Set Action ==>" + str(action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        To know which Variables we have acces to, we need to read the
        TurtleBot2Env API DOCS
        :return:
        """
        rospy.logdebug("Start Get Observation ==>")

        self.episode_step += 1
        rospy.logdebug("Number of steps ==>" + str(self.episode_step))

        # We get the laser scan data
        laser_scan = self.get_laser_scan()

        # check if robot has crashed

        discretized_observations = self.discretize_scan_observation(laser_scan,self.new_ranges)

        current_image, current_pixel_coords = self.get_image_coordinate_points()
        cv2.imshow("Current Image", current_image)
        cv2.waitKey(1)

        self.current_image = current_image

        if self.episode_step == self.max_episode_steps:
            rospy.logerr("max epi steps done")
            self._episode_done = True

        if self._episode_done:
            obs_spaces = {'goal_image': self.goal_image, 'current_image': current_image}
            return obs_spaces
        else:

            self.calculateMeanDistance(self.goal_coordinate_points, current_pixel_coords)

        #rospy.logdebug("Observations==> Number of red pixels: " + str(discretized_observations))
        rospy.logdebug("END Get Observation ==>")

        obs_spaces = {'goal_image': self.goal_image, 'current_image': current_image}
        return obs_spaces



    def _is_done(self, observations):

        if self._episode_done:
            rospy.logerr("Episode done ==>")
            self._episode_done = True
            self.previous_mean_distance = self.current_mean_distance

            # Now we check if it has crashed based on the imu
            imu_data = self.get_imu()
            linear_acceleration_magnitude = self.get_vector_magnitude(imu_data.linear_acceleration)
            if linear_acceleration_magnitude < self.max_linear_aceleration:
                rospy.logerr("TurtleBot3 Crashed==>" + str(linear_acceleration_magnitude) + ">" + str(
                    self.max_linear_aceleration))
                self._episode_done = True
            else:
                rospy.logerr("DIDNT crash TurtleBot3 ==>" + str(linear_acceleration_magnitude) + "<" + str(
                    self.max_linear_aceleration))
                # self._episode_done = False'''

        else:
            self._episode_done = False
            rospy.logwarn("Episode not done ==>")

        return self._episode_done

    def _compute_reward(self, observations, done):

        if not done:
            if self.previous_mean_distance < self.current_mean_distance:
                reward = self.increased_distance_points_reward
            else:
                reward =  self.decreased_distance_points_reward
        else:
            if round(self.current_mean_distance,1) <= self.threshold_goal_distance:
                reward = self.goal_reached_points
            else:
                if self.episode_step == self.max_episode_steps:
                    rospy.logerr("Max episode steps reached ==>")
                    reward = self.max_episode_steps_points
                else:
                    reward = -1 * self.end_episode_points

        rospy.logdebug("reward=" + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward=" + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps=" + str(self.cumulated_steps))
        # assign previous number of pixels to current for next step
        self.previous_mean_distance = self.current_mean_distance

        return reward

    # Internal TaskEnv Methods


    def calculateMeanDistance(self, goal_coordinate_points, current_coordinate_points):
        """
        Calculates the mean distance between the four corresponding corner points of the desired image and the current image
        """
        top_left = math.sqrt((goal_coordinate_points[0][0] - current_coordinate_points[0][0]) ** 2 + (goal_coordinate_points[0][1] - current_coordinate_points[0][1]) ** 2)
        top_right = math.sqrt((goal_coordinate_points[1][0] - current_coordinate_points[1][0]) ** 2 + (goal_coordinate_points[1][1] - current_coordinate_points[1][1]) ** 2)
        bottom_left = math.sqrt((goal_coordinate_points[2][0] - current_coordinate_points[2][0]) ** 2 + (goal_coordinate_points[2][1] - current_coordinate_points[2][1]) ** 2)
        bottom_right = math.sqrt((goal_coordinate_points[3][0] - current_coordinate_points[3][0]) ** 2 + (goal_coordinate_points[3][1] - current_coordinate_points[3][1]) ** 2)
        mean_distance = (top_left + top_right + bottom_left + bottom_right)/4
     
        self.mean_distance.append(mean_distance)
        rospy.loginfo("Mean distance")
        rospy.loginfo(self.mean_distance)

        self.current_mean_distance = mean_distance
        if self.current_mean_distance < self.threshold_goal_distance:
            rospy.loginfo("Goal reached")
            self._episode_done = True
        rospy.loginfo("Current mean distance=" + str(self.current_mean_distance))
        return mean_distance

    def discretize_scan_observation(self, data, new_ranges):
        """
        Discards all the laser readings that are not multiple in index of new_ranges
        value.
        """
        self._episode_done = False

        discretized_ranges = []
        mod = len(data.ranges) / new_ranges

        # rospy.logdebug("data=" + str(data))
        rospy.logdebug("new_ranges=" + str(new_ranges))
        rospy.logdebug("mod=" + str(mod))

        for i, item in enumerate(data.ranges):
            if (i % mod == 0):
                if item == float('Inf') or numpy.isinf(item):
                    discretized_ranges.append(self.max_laser_value)
                elif numpy.isnan(item):
                    discretized_ranges.append(self.min_laser_value)
                else:
                    if self.learning_algorithm == 'deepRL':
                        discretized_ranges.append(item)
                    else:
                        discretized_ranges.append(int(item))

            if (self.min_range > item > 0):
                rospy.logerr("done Validation >>> item=" + str(item) + "< " + str(self.min_range))
                self._episode_done = True
            # else:
            # rospy.logdebug("NOT done Validation >>> item=" + str(item)+"< "+str(self.min_range))

        if self.learning_algorithm == 'deepRL':
            return np.array(discretized_ranges).astype(np.float32)
        else:
            return discretized_ranges

    def get_vector_magnitude(self, vector):
        """
        It calculated the magnitude of the Vector3 given.
        This is usefull for reading imu accelerations and knowing if there has been
        a crash
        :return:
        """
        contact_force_np = numpy.array((vector.x, vector.y, vector.z))
        force_magnitude = numpy.linalg.norm(contact_force_np)

        return force_magnitude




